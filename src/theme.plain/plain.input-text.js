import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import TegInputText from '../core/raw/teg.input-text';

class PlainInputText extends React.PureComponent {
  render() {
    const {
      tegPrimaryLabel,
      tegSecondaryLabel,
      tegStyle = {},
      tegInputTextStyle,
      tegInputTextProps,
      ...rest
    } = this.props;


    const tegLabelSize = tegStyle.tegLabelSize || 10;
    const tegTextSize = tegStyle.tegTextSize || 12;
    const tegBackgroundColor = tegStyle.tegBackgroundColor;
    const tegPlaceholderColor = tegStyle.tegPlaceholderColor || tegStyle.tegDecorationColor;
    const tegDecorationColor = tegStyle.tegDecorationColor;
    const tegTextColor = tegStyle.tegTextColor;
    const tegFontFamily = tegStyle.tegFontFamily;

    const tegHorizontalPadding = tegStyle.tegHorizontalPadding || 16;
    const tegVerticalPadding = tegStyle.tegVerticalPadding || 8;
    const tegBorderRadius = tegStyle.tegBorderRadius || 4;
    const tegBorderWidth = tegStyle.tegBorderWidth || 2;

    const inputContainerStyle = ({ 
      marginTop: tegLabelSize * 0.55,
      marginBottom: tegLabelSize * 0.55,
      backgroundColor: tegBackgroundColor, 
      borderWidth: tegBorderWidth,
      borderColor: tegDecorationColor, 
      borderRadius: tegBorderRadius,
      justifyContent: 'center'
    });

    const inputStyle = [{
      color: tegTextColor,
      fontSize: tegTextSize,
      paddingLeft: tegHorizontalPadding,
      paddingRight: tegHorizontalPadding,
      paddingTop: tegVerticalPadding,
      paddingBottom: tegVerticalPadding,
    }, tegInputTextStyle];

    const secondaryLabelStyle = ({
      alignSelf: 'flex-start',
      position: 'absolute',
      left: tegHorizontalPadding - (tegHorizontalPadding / 3),
      bottom: tegLabelSize / 2 * -1,
      paddingLeft: tegHorizontalPadding / 3,
      paddingRight: tegHorizontalPadding / 3,
      fontSize: tegLabelSize,
      paddingTop: 0,
      backgroundColor: tegBackgroundColor,
      color: tegDecorationColor,
      height: tegLabelSize,
      lineHeight: tegLabelSize
    });

    const primaryLabelStyle = ({
      alignSelf: 'flex-start',
      position: 'absolute',
      left: tegHorizontalPadding - (tegHorizontalPadding / 4),
      top: tegLabelSize / 2 * -1,
      paddingLeft: tegHorizontalPadding / 4,
      paddingRight: tegHorizontalPadding / 4,
      fontSize: tegLabelSize,
      paddingTop: 0,
      backgroundColor: tegBackgroundColor,
      color: tegDecorationColor,
      height: tegLabelSize,
      lineHeight: tegLabelSize
    });

    return (
      <View style={inputContainerStyle}>
        <TegInputText
          style={inputStyle}
          tegPlaceholderColor={tegPlaceholderColor}
          {...rest}
          {...tegInputTextProps}
        />
        { !tegPrimaryLabel ? null : <Text style={primaryLabelStyle}>{tegPrimaryLabel}</Text> }
        { !tegSecondaryLabel ? null : <Text style={secondaryLabelStyle}>{tegSecondaryLabel}</Text> }
      </View>
    );
  }
}


PlainInputText.propTypes = {
  tegValue:                       PropTypes.string.isRequired,
  tegPrimaryLabel:                PropTypes.string.isRequired,
  tegSecondaryLabel:              PropTypes.string,
  tegPlaceholder:                 PropTypes.string.isRequired,
  tegMaxLength:                   PropTypes.number,
  tegEnabled:                     PropTypes.bool,
  
  tegMask:                        PropTypes.oneOfType([PropTypes.func, PropTypes.array, PropTypes.string]),
  
  tegStyle: PropTypes.PropTypes.shape({
    tegDecorationColor:           PropTypes.string.isRequired,
    tegTextColor:                 PropTypes.string.isRequired,
    tegBackgroundColor:           PropTypes.string.isRequired,
    tegLabelSize:                 PropTypes.number,
    tegTextSize:                  PropTypes.number,
    tegFontFamily:                PropTypes.string,
    tegHorizontalPadding:         PropTypes.number,
    tegVerticalPadding:           PropTypes.number,
  }).isRequired,

  tegContainerStyle:              PropTypes.any,
  tegInputTextStyle:              PropTypes.any,
  
  tegDidChangeValue:              PropTypes.func.isRequired,
  tegDidFocus:                    PropTypes.func,
  tegDidBlur:                     PropTypes.func,

  tegSecureTextEntry:             PropTypes.bool,
  tegInputContentType:            PropTypes.string,
  tegKeyboardType:                PropTypes.string,
  tegReturnKeyType:               PropTypes.string,

  tegName:                        PropTypes.string.isRequired,
  tegCategory:                    PropTypes.string.isRequired,
  tegAutoFocus:                   PropTypes.bool,
  tegNextFocus:                   PropTypes.string,
};


PlainInputText.defaultProps = {
};


export default PlainInputText;
