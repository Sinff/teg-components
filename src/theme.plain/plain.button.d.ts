import React from 'react';

export interface PlainButtonStyle {
  tegBackgroundColor: string;
  tegTextColor: string;
  tegTextSize: string;
  tegBorderRadius: number;
  tegPadding: number;
}

export interface PlainButtonProps {
  tegStyle: PlainButtonStyle;

  tegDidPress: () => void;

  tegText?: string;

  tegIconName?: string;
  tegIconSource?: string;
  tegIconSize?: number;
  tegIconGap?: number;

  tegState: 'LOADING' | 'DEFAULT' | 'DISABLED';
  tegType: 'DEFAULT' | 'OUTLINE';
}

export interface PlainButtonExtra {
}

export default class PlainButton extends React.Component<PlainButtonProps> {}
