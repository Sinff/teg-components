import React from 'react';

export interface Option {
  key: string;
  value: string;
  description: string;
}

export interface PlainInputPickerExtra {
  tegPickerIcon: { tegSource: string, tegIcon: string  }
}

export interface PlainInputPickerStyle {
  tegDecorationColor:           string,
  tegTextColor:                 string,
  tegBackgroundColor:           string,
  tegPlaceholderColor: string;
  tegLabelSize:                 number,
  tegTextSize:                  number,
  tegFontFamily:                string,
  tegHorizontalPadding:         number,
  tegVerticalPadding:           number,
  tegBorderRadius:              number;
  tegBorderWidth:               number;
  
  tegPickerTitleSize:           number;
  tegPickerHorizontalPadding:   number;
  tegPickerSeparatorSize:       number;

  tegOpenPickerIcon?: {
    tegSource?: string;
    tegIcon?: string;
  };

  tegClosePickerIcon?: {
    tegSource?: string;
    tegIcon?: string;
  }
}

export interface PlainInputPickerProps {

  tegTheme?: string;
  
  /** Input placeholder */
  tegPlaceholder: string;
  /** Input label */
  tegPrimaryLabel: string;
  tegSecondaryLabel: string;
  
  tegSelectedOption?: Option,
  
  tegOptions?: Option[];

  tegOptionPickerView: (props: any) => any;
  tegSelectedOptionView: (props: any) => any;
 
  tegIsPicking: boolean;


  /** Function called when the value has changed */
  tegDidSelectOption?: (item: Option) => void;
  /** Function called on focus */
  tegDidFocus?: () => void;
  /** Function called on blur */
  tegDidBlur?: () => void;

  /** Function called to retrieve the masked value */
  tegMask?: (value: string) => string;

  tegEnabled?: boolean;

  /** The component unique name (Required) */
  tegName: string;
  /** The component focus category. It is used to group inputs in a form (Required) */
  tegCategory: string;
  /** Whether or not the component should request focus on mount */
  tegAutoFocus?: boolean;
  /** The next component to receive focus on blur */
  tegNextFocus?: boolean;

  tegStyle?: PlainInputPickerStyle;
}

export default class PlainInputPicker extends React.Component<PlainInputPickerProps> {}
