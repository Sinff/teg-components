import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableHighlight, FlatList, Dimensions, TouchableWithoutFeedback } from 'react-native';
import TegFrameOverlay from '../core/overlay/teg.frame-overlay';
import { addFocusable, delFocusable, doBlur, doFocus, doFocusNext } from '../core/services/srv.focus';
import { getBottomSpace, getStatusBarHeight } from 'react-native-iphone-x-helper';
import PlainIcon from '../core/raw/teg.icon';
import TegGap from '../core/components/teg.gap';

class PlainInputPicker extends React.Component {

  constructor(props) {
    super(props);

    this.state = { 
      isPicking: false,
      focused: false,
    };

    this.didTouchOpenPicker = this.didTouchOpenPicker.bind(this);
    this.didFinishOptionSelection = this.didFinishOptionSelection.bind(this);
    this.didFocus = this.didFocus.bind(this);
    this.didBlur = this.didBlur.bind(this);
  }

  componentDidMount() {
    addFocusable(
      this.props.tegCategory,
      {
        id: this.props.tegName,
        focused: this.props.tegAutoFocus,
        didFocus: this.didFocus,
        didBlur: this.didBlur,
        next: this.props.tegNextFocus
      }
    );
  }

  componentWillUnmount() {
    delFocusable(this.props.tegCategory, this.props.tegName);
  }

  didFocus() {
    if(!this.props.tegEnabled)
      return doFocusNext(this.props.tegCategory, this.props.tegName);

    if(this.state.focused)
      return;

    this.setState({isPicking: true, focused: true});
    this.props.didFocus && this.props.didFocus();
  }

  didBlur() {
    if(!this.state.focused)
      return;

    this.setState({isPicking: false, focused: false});
  }

  didTouchOpenPicker() {
    if(!this.props.tegEnabled)
      return;

    if(this.state.focused && !this.state.isPicking)
      this.setState({isPicking: true});
    else
      doFocus(this.props.tegCategory, this.props.tegName);
  }

  didFinishOptionSelection(opt) {
    if(!opt) {
      this.setState({isPicking: false});
      return doBlur(this.props.tegCategory, this.props.tegName);
    } 

    this.setState({isPicking: false});
    this.props.tegDidSelectOption(opt);

    doFocusNext(this.props.tegCategory, this.props.tegName);
  }

  render() {
    const { 
      tegPlaceholder,
      tegPrimaryLabel,  
      tegSecondaryLabel, 
      tegEnabled,
      tegOptions, 
      tegSelectedOption,
      tegStyle = {},
    } = this.props;
   
    const { PickerView } = this;
    const tegPickerIcon = this.props.tegPickerIcon;
            
    const tegBackgroundColor = tegStyle.tegBackgroundColor;
    const tegDecorationColor = tegStyle.tegDecorationColor;
    const tegTextColor = tegStyle.tegTextColor;
    const tegPlaceholderColor = tegStyle.tegPlaceholderColor || tegStyle.tegDecorationColor;

    const tegTextSize = tegStyle.tegTextSize || 12;
    const tegLabelSize = tegStyle.tegLabelSize || 10;
    const tegFontFamily = tegStyle.tegFontFamily;

    const tegHorizontalPadding = tegStyle.tegHorizontalPadding || 16;
    const tegVerticalPadding = tegStyle.tegVerticalPadding || 8;
    const tegBorderRadius = tegStyle.tegBorderRadius || 4;
    const tegBorderWidth = tegStyle.tegBorderWidth || 2;

    
    const containerStyle = {
      marginTop: tegLabelSize * 0.55,
      marginBottom: tegLabelSize * 0.55,
      backgroundColor: tegBackgroundColor, 
      borderWidth: tegBorderWidth,
      borderColor: tegDecorationColor, 
      borderRadius: tegBorderRadius,
      justifyContent: 'center',
    };
    
    const touchableStyle = {
      backgroundColor: tegBackgroundColor,
      borderRadius: tegBorderRadius,
    };
    
    const contentStyle = {
      flex: 1, 
      flexDirection: 'row',
      backgroundColor: tegBackgroundColor,
      borderRadius: tegBorderRadius,
      alignItems: 'stretch',
      justifyContent: 'space-between',  
      paddingTop: tegVerticalPadding + 5,
      paddingBottom: tegVerticalPadding + 5,    
      paddingLeft: tegHorizontalPadding,
      paddingRight: tegHorizontalPadding,
    };

    const pickerIconStyle = {
      alignSelf: 'center',
      width: tegTextSize, 
      height: tegTextSize, 
    };

    const primaryLabelStyle = {
      position: 'absolute',
      left: tegHorizontalPadding - (tegHorizontalPadding > 4 ? 4 : 0),
      top: tegLabelSize / 2 * -1,
      paddingLeft: tegHorizontalPadding / 3,
      paddingRight: tegHorizontalPadding / 3,
      fontSize: tegLabelSize,
      paddingTop: 0,
      backgroundColor: tegBackgroundColor,
      color: tegDecorationColor,
      height: tegLabelSize,
      lineHeight: tegLabelSize,
      borderRadius: 2,
    };

    const secondaryLabelStyle = {
      alignSelf: 'flex-start',
      position: 'absolute',
      borderRadius: 2,
      left: tegHorizontalPadding - (tegHorizontalPadding / 3),
      bottom: tegLabelSize / 2 * -1,
      paddingLeft: tegHorizontalPadding / 3,
      paddingRight: tegHorizontalPadding / 3,
      fontSize: tegLabelSize,
      paddingTop: 0,
      backgroundColor: tegBackgroundColor,
      color: tegDecorationColor,
      height: tegLabelSize,
      lineHeight: tegLabelSize
    };

    const hasSelectedItem = !!tegSelectedOption && !!tegSelectedOption.key; 
    const contentText = hasSelectedItem ? tegSelectedOption.value : tegPlaceholder; 
    const contentTextStyle = {
      color: hasSelectedItem ? tegTextColor : tegPlaceholderColor,
      fontSize: tegTextSize,
    }
 
    return (
      <View style={containerStyle}>
        <PickerView
          tegEnabled={tegEnabled}
          tegPickerIcon={tegPickerIcon}
          isPicking={this.state.isPicking}
          placeholder={tegPlaceholder}
          didSelectOption={this.didFinishOptionSelection}
          didCancelSelection={() => this.didFinishOptionSelection()}
          options={tegOptions}
          tegStyle={tegStyle}
        />

        <TouchableHighlight 
          onPress={this.didTouchOpenPicker} 
          style={touchableStyle}
        >
          <View style={contentStyle}>
            <View style={{flex: 1, justifyContent: 'center'}}>
              <Text 
                style={contentTextStyle} 
                numberOfLines={1} 
                ellipsizeMode={"tail"} 
              >{contentText}</Text>

            </View>

            <View style={pickerIconStyle}>
              <PlainIcon
                tegIcon={tegPickerIcon.tegIcon}
                tegSource={tegPickerIcon.tegSource}
                tegSize={tegPickerIcon.tegSize || tegTextSize} 
                tegColor={tegPickerIcon.tegColor || tegDecorationColor} />
            </View>

            { 
              !!tegSecondaryLabel
              ? <Text style={secondaryLabelStyle} numberOfLines={1}  ellipsizeMode="tail" >{tegSecondaryLabel}</Text>
              : null
            }
          </View>
        </TouchableHighlight>

        {
          !tegPrimaryLabel
            ? null
            : <Text 
                style={primaryLabelStyle} 
                numberOfLines={1}
                ellipsizeMode="tail"
              >{tegPrimaryLabel}</Text>
        }

      </View>
    )
  }

  PickerView(props) {
    const {  
      didSelectOption, 
      didCancelSelection,
      placeholder, 
      options, 
      isPicking,
      tegPickerIcon,
      tegStyle,
    } = props;
    

    const tegBackgroundColor = tegStyle.tegBackgroundColor;
    const tegTextColor = tegStyle.tegTextColor;
    const tegTextSize = tegStyle.tegTextSize;
    const tegLabelSize = tegStyle.tegLabelSize;
    const tegHorizontalPadding = tegStyle.tegHorizontalPadding;
    
    const tegPickerTitleSize = tegStyle.tegPickerTitleSize || 14;
    const tegPickerItemHorizontalPadding = tegStyle.tegPickerItemHorizontalPadding;
    const tegPickerItemVerticalPadding = tegStyle.tegPickerItemVerticalPadding;
    const tegPickerItemSeparatorSize = tegStyle.tegPickerSeparatorSize;
    const tegPickerRadius = tegStyle.borderRadius || 4;
    const tegPickerTitleHorizontalPadding = tegStyle.tegPickerTitleHorizontalPadding || 16;
    const tegPickerTitleVerticalPadding = tegStyle.tegPickerTitleVerticalPadding || 8;

    const containerStyle = {
      backgroundColor: tegBackgroundColor,
      borderTopRightRadius: tegPickerRadius,
      borderTopLeftRadius: tegPickerRadius,
    };

    const itemContainerStyle = {
      backgroundColor: tegBackgroundColor,
      paddingTop: tegPickerItemVerticalPadding,
      paddingBottom: tegPickerItemVerticalPadding,
      paddingLeft: tegPickerItemHorizontalPadding,
      paddingRight: tegPickerItemHorizontalPadding,
      flex: 1,
    };

    const itemBorderTop = {
      backgroundColor: tegStyle.tegPlaceholderColor,
      height: tegPickerItemSeparatorSize,
      flex: 1,
    };

    const mainTextStyle = {
      color: tegTextColor,
      fontSize: tegTextSize,
      fontWeight: 'bold'
    };   

    const subTextStyle = {
      marginTop: 4,
      color: tegStyle.tegPlaceholderColor,
      fontSize: tegLabelSize,
    };

    const closeButtonStyle = {
      height: 40,
      width: 40,
      backgroundColor: tegBackgroundColor,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 20
    };

    const titleStyle = {
      fontSize: tegPickerTitleSize,
      color: tegStyle.tegTextColorOnFocus,
      marginTop: tegTextSize * 0.4,
      marginLeft: tegHorizontalPadding
    };

    const headerContainerStyle = {
      paddingTop: tegPickerTitleVerticalPadding,
      paddingLeft: tegPickerTitleHorizontalPadding,
      paddingRight: tegPickerTitleHorizontalPadding - 12,
      paddingBottom: tegPickerTitleVerticalPadding,
      flexDirection: 'row',
      justifyContent: 'space-between'
    };

    const maxHeight = Dimensions.get('screen').height - 100 - getStatusBarHeight();

    return (
      <TegFrameOverlay
        tegContentAlignment={'flex-end'} 
        tegIsShowing={isPicking} 
        tegDidTouchCloseDialog={didCancelSelection}  
      >
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'flex-end' }}>
          <TouchableWithoutFeedback style={{flex: 1}} onPress={didCancelSelection}>
            <View style={{ backgroundColor: 'transparent', flex: 1 }}></View>
          </TouchableWithoutFeedback>
          <View style={[containerStyle, {maxHeight}]}>
            <View style={[headerContainerStyle]}>
              <View style={{flex: 1}}>
                <Text style={titleStyle} numberOfLines={1} ellipsizeMode="tail" >{placeholder}</Text>
              </View>

              <TouchableHighlight onPress={didCancelSelection} style={closeButtonStyle}>
                <View style={closeButtonStyle}>
                  <PlainIcon 
                    tegIcon={tegPickerIcon.tegIcon}
                    tegSource={tegPickerIcon.tegSource}    
                    tegSize={(tegPickerIcon.tegSize || titleStyle.fontSize) * 1.8} 
                    tegColor={tegPickerIcon.tegColor || tegStyle.tegDecorationColor}
                  />
                </View>
              </TouchableHighlight>
            </View>

            <FlatList
              data={options}
              keyExtractor={(item) => item.key}
              renderItem={({item, index}) => 
                <TouchableHighlight style={{flex:1}} onPress={() => didSelectOption(item)}>
                  <View style={{flex: 1, flexDirection: 'column'}}>
                    <View style={index > 0 ? itemBorderTop : {}}></View>
                    <View style={[itemContainerStyle]}>
                      <Text style={mainTextStyle}>{item.value}</Text>
                      { !item.description ? null : <Text style={subTextStyle}>{item.description}</Text> }
                    </View>
                  </View>
                </TouchableHighlight>
              }
            />
            <TegGap tegGap={getBottomSpace() + 12} />
          </View>
        </View>
      </TegFrameOverlay>
    );
  }
}


PlainInputPicker.propTypes = { 
  tegPlaceholder: PropTypes.string.isRequired,
  tegPrimaryLabel: PropTypes.string.isRequired,
  tegSecondaryLabel: PropTypes.string,
  
  tegEnabled: PropTypes.bool,
  
  tegOptions: PropTypes.arrayOf(PropTypes.shape({
    key: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    description: PropTypes.string
  })).isRequired,

  tegDidSelectOption: PropTypes.func.isRequired,
  tegDidFocus: PropTypes.func,
  tegDidBlur: PropTypes.func,

  tegValidator: PropTypes.func,
  tegMask: PropTypes.func,

  tegName: PropTypes.string.isRequired,
  tegCategory: PropTypes.string.isRequired,
  tegAutoFocus: PropTypes.bool,
  tegNextFocus: PropTypes.string,
  
  tegSubmitEditing: PropTypes.func,
  tegPickerIcon: PropTypes.shape({
    tegSource: PropTypes.string,
    tegIcon: PropTypes.string.isRequired,
    tegSize: PropTypes.number
  }),

  tegStyle: PropTypes.PropTypes.shape({
    tegDecorationColor:             PropTypes.string.isRequired,
    tegTextColor:                   PropTypes.string.isRequired,
    tegBackgroundColor:             PropTypes.string.isRequired,
    tegLabelSize:                   PropTypes.number,
    tegTextSize:                    PropTypes.number,
    tegFontFamily:                  PropTypes.string,
    tegHorizontalPadding:           PropTypes.number,
    tegVerticalPadding:             PropTypes.number,

    tegPickerItemHorizontalPadding: PropTypes.number,
    tegPickerItemVerticalPadding:   PropTypes.number,
    tegPickerTitleHorizontalPadding:PropTypes.number,
    tegPickerTitleVerticalPadding:  PropTypes.number,
    tegPickerTitleSize:             PropTypes.number,
  }).isRequired,
  
  tegSelectedOption: PropTypes.shape({
    key: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    description: PropTypes.string
  })
};

PlainInputPicker.defaultProps = {
  tegEnabled: true,
  tegOptions: []
}

export default PlainInputPicker;
