import React from 'react';
import TegInputText, {TegInputTextProps} from '../core/raw/teg.input-text';


interface PlainInputTextProps extends TegInputTextProps {
  tegPrimaryLabel: string;
  tegSecondaryLabel: string;

  tegStyle: {
    tegDecorationColor:   string;
    tegTextColor:         string;
    tegBackgroundColor:   string;
    tegLabelSize:         number;
    tegTextSize:          number;
    tegFontFamily:        string;
    tegHorizontalPadding: number;
    tegVerticalPadding:   number;
    tegBorderWidth:       number;
  }
}

export default class PlainInputText extends React.PureComponent<PlainInputTextProps> {};
