import React from 'react';
import { View, TouchableHighlight, Text } from 'react-native';
import PropTypes from 'prop-types';
import PlainIcon from '../core/raw/teg.icon';
import { BallIndicator } from 'react-native-indicators';

class PlainButton extends React.Component {
  render() { 
    const { 
      tegStyle = {},
      tegIconName,
      tegIconSource,
      tegIconSize,
      tegIconGap,
      tegState,
      tegText,
      tegDidPress,
      tegType
    } = this.props;

    const STL = { 
      tegBackgroundColor: tegStyle.tegBackgroundColor,
      tegTextColor: tegStyle.tegTextColor,
      tegTextSize: tegStyle.tegTextSize,
      tegBorderRadius: tegStyle.tegBorderRadius,
      tegPadding: tegStyle.tegPadding,
    };
  
    const tegBackgroundColor = tegType == 'DEFAULT' ? STL.tegBackgroundColor : STL.tegTextColor;
    const tegTextColor = tegType == 'DEFAULT' ? STL.tegTextColor : STL.tegBackgroundColor;
    const tegBorder = tegType == 'DEFAULT' ? {} : { borderWidth: 2, borderColor: tegTextColor };

    const containerStyle = {
      borderRadius: STL.tegBorderRadius || 0,
      justifyContent: 'center',
      alignItems: 'center',
      padding: STL.tegPadding,
      backgroundColor: tegBackgroundColor,
      ...tegBorder
    };

    const inTextStyle = {
      marginLeft: !!tegIconName ? tegIconGap : 0,
      fontSize: STL.tegTextSize,
      color: tegTextColor,
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center'
    };

    const didPress = tegState === STATE_DISABLED ? undefined 
      : tegState === STATE_LOADING ? () => {}
      : tegState === STATE_DEFAULT ? tegDidPress
      : undefined;

    return (
      <TouchableHighlight onPress={didPress} style={{borderRadius: STL.tegBorderRadius}}>
        <View style={[containerStyle]}>
          { tegState !== STATE_LOADING
            ? null
            : <BallIndicator size={tegIconSize || STL.tegTextSize * 1.35} color={tegTextColor} style={{
                alignItems: 'center',
                justifyContent: 'center',
                textAlign: 'center'              
              }} />
          }

          { !tegIconName || tegState === STATE_LOADING
            ? null
            : <PlainIcon 
                tegIcon={tegIconName} 
                tegSource={tegIconSource} 
                tegColor={tegTextColor}
                tegTheme={tegTheme}
                tegSize={tegIconSize || STL.tegTextSize * 1.35}
              ></PlainIcon>
          }
        
          { !tegText || tegState === STATE_LOADING
            ? null
            : <Text style={[inTextStyle]}>{(tegText || '').toUpperCase()}</Text>
          }
        </View>
      </TouchableHighlight>
    )
  }

}

export const STATE_LOADING = 'LOADING';
export const STATE_DISABLED = 'DISABLED';
export const STATE_DEFAULT = 'DEFAULT';

PlainButton.propTypes = { 
  tegType:        PropTypes.oneOf(['DEFAULT', 'OUTLINE']),

  tegDidPress:    PropTypes.func.isRequired,

  tegText:        PropTypes.string,

  tegIconName:    PropTypes.string,
  tegIconSource:  PropTypes.string,
  tegIconSize:    PropTypes.number,
  tegIconGap:     PropTypes.number,

  tegState:       PropTypes.oneOf([STATE_DEFAULT, STATE_LOADING, STATE_DISABLED]).isRequired
};

PlainButton.defaultProps = {
  tegState: STATE_DEFAULT,
  tegType: 'DEFAULT'
}

export default PlainButton;
