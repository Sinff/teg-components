export function isNullOrUndefined(val) { return val == null || val == undefined; }
export function isArray(val) { return Array.isArray(val); }
export function isString(val) { return typeof val === typeof '' }
export function isObject(val) { return !isNullOrUndefined(val) && !isArray(val) && typeof val == 'object' }
export function firstValid(values, validate) { 
  values = values || [];

  for(let i = 0; i < values.length; i++)
    if(validate(values[i]))
      return values[i];
      
  return undefined
}
