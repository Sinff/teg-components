import React from 'react';
import { Dimensions, Animated } from 'react-native';
import PropTypes from 'prop-types';

class AniBottomView extends React.Component {
  constructor(props) {
    super(props);
    this.getScreenHeight = this.getScreenHeight.bind(this);
    this.posY = new Animated.Value(this.getScreenHeight());
  }

  getScreenHeight() {
    return Dimensions.get('screen').height;
  }

  componentDidMount() {
    Animated.timing(this.posY, {
      toValue: this.props.isShowing ? 0 : this.getScreenHeight(),
      duration: this.props.duration,
    }).start(this.props.didFinishAnimation);
  }

  componentDidUpdate(prevProps) {    
    if(prevProps.isShowing  != this.props.isShowing) {
      Animated.timing(this.posY, {
        toValue: this.props.isShowing ? 0 : this.getScreenHeight(),
        duration: this.props.duration,
      }).start(this.props.didFinishAnimation);
    }
  }

  render() {
    const { isShowing, style, children, duration, didFinishAnimation, ...rest } = this.props;
    const combinedStyle = [style, { top: this.posY }];
   
    return (<Animated.View style={combinedStyle} {...rest}>{children}</Animated.View>);
  }
}

AniBottomView.propTypes = {
  duration: PropTypes.number.isRequired,
  isShowing: PropTypes.bool.isRequired,
  didFinishAnimation: PropTypes.func
}

AniBottomView.defaultProps = {
  duration: 500,
  isShowing: false
}

export default AniBottomView;
