import React from 'react';
import PropTypes from 'prop-types';
import { Animated } from 'react-native';


class AniFadeView extends React.Component {

  constructor(props) {
    super(props);
    this._visibility = new Animated.Value(0);
    this.didFinishAnimation = this.didFinishAnimation.bind(this);
  }

  didFinishAnimation(params) {
    this.props.didFinishAnimation && this.props.didFinishAnimation(params);
  }

  componentDidMount() {
    Animated.timing(this._visibility, {
      toValue: this.props.isShowing ? 1 : 0,
      duration: this.props.duration,
    }).start(() => this.didFinishAnimation({
      type: this.props.isShowing ? 'IN' : 'OUT'
    }));
  }

  componentDidUpdate(prevProps) {
    if(prevProps.isShowing != this.props.isShowing)
      Animated.timing(this._visibility, {
        toValue: this.props.isShowing ? 1 : 0,
        duration: this.props.duration,
        delay: this.props.delay
      }).start(() => this.didFinishAnimation({
        type: this.props.isShowing ? 'IN' : 'OUT'
      }));  
  }

  render() {
    const { isShowing, style, children } = this.props;

    const containerStyle = { opacity: this._visibility };
    const combinedStyle = [containerStyle, style];
    
    return (
      <Animated.View style={combinedStyle}>
        {children}
      </Animated.View>
    );
  }
}

AniFadeView.propTypes = {
  isShowing: PropTypes.bool.isRequired,
  duration: PropTypes.number.isRequired,
  delay: PropTypes.number.isRequired,
  didFinishAnimation: PropTypes.func
}

AniFadeView.defaultProps = {
  duration: 500,
  delay: 0
}

export default AniFadeView;
