import React from 'react';
import { Animated, View, Dimensions } from 'react-native';
import PropTypes from 'prop-types';


class TegCollapsableScrollHeader extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0)
    };
  }
  
  render = () => {
    const {
      tegTopLeftSection: TegTopLeftSection,
      tegTopRightSection: TegTopRightSection,
      
      tegScalableSection: TegScalableSection,
      tegScalableSectionExpandedHeight,
      tegScalableSectionCollapsedHeight,
      
      tegFadeableSection: TegFadeableSection,
      tegFadeableSectionHeight,

      tegDividerSection: TegDividerSection,
      tegDividerHeight,

      children,
      style,

      tegLock,

      tegStyle = {},
      tegDividerStyle
    } = this.props;

    const { 
      tegBackgroundColor,  
      tegPaddingLeft,
      tegPaddingRight,
      tegPaddingTop,
      tegPaddingBottom,
      tegPadding
    } = tegStyle;

    // CONSTANT VALUES
    const scalableColHeight = tegScalableSectionCollapsedHeight;
    const scalableExpHeight = tegScalableSectionExpandedHeight;
    const scalableExpOffset = scalableColHeight * 0.75;

    const headerLeftGap = (tegPaddingLeft || tegPadding || 0);
    const headerRightGap = (tegPaddingRight || tegPadding || 0);
    const headerTopGap = (tegPaddingTop || tegPadding || 0);
    const headerBottomGap = (tegPaddingBottom || tegPadding || 0);

    const screenWidth = Dimensions.get('screen').width;
    const windowHeight = Dimensions.get('window').height;

    const headerExpHeight = (tegFadeableSectionHeight || 0)
      + (tegScalableSectionExpandedHeight || 0)
      + (headerTopGap)
      + (scalableExpOffset)
      + (headerBottomGap)
      + (tegDividerHeight || 0);

    const headerColHeight = (tegScalableSectionCollapsedHeight)
      + (headerTopGap)
      + (headerBottomGap)
      + (tegDividerHeight || 0);


    // HEADER CONTAINER
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, headerExpHeight - headerColHeight],
      outputRange: tegLock === 'COLLAPSED'
        ? [headerColHeight, headerExpHeight]
        : [headerExpHeight, headerColHeight],
      extrapolate: 'clamp'
    });

    const headerAnimStyle = {
      height: headerHeight, 
      width: screenWidth,
    };

    // SCALABLE SECTION
    const scalableSize = this.state.scrollY.interpolate({
      inputRange:  [0, headerExpHeight - headerColHeight],
      outputRange: tegLock === 'COLLAPSED' 
        ? [scalableColHeight / scalableExpHeight, 1] 
        : [1, scalableColHeight / scalableExpHeight],
      extrapolate: 'clamp'
    });

    const scalableAnimStyle = {
      alignItems: 'center',
      bottom: 0,
      marginLeft: headerLeftGap,
      marginRight: headerRightGap,
      transform: [{ 
        scale: scalableSize 
      }],
    };

    const scalableTranslateY = this.state.scrollY.interpolate({
      inputRange:  [0, headerExpHeight - headerColHeight],
      outputRange: tegLock === 'COLLAPSED' 
        ? [ headerTopGap, scalableExpOffset + headerTopGap ]
        : [ scalableExpOffset + headerTopGap, headerTopGap ],
      extrapolate: 'clamp'
    });

    const scalableHeight = this.state.scrollY.interpolate({
      inputRange:  [0, headerExpHeight - headerColHeight],
      outputRange: tegLock === 'COLLAPSED' 
        ? [scalableColHeight, scalableExpHeight]
        : [scalableExpHeight, scalableColHeight],
      extrapolate: 'clamp'
    });

    const scalableContainerAnimStyle = {
      position: 'absolute', 
      top: 0, 
      transform: [{ translateY: scalableTranslateY }],
      width: screenWidth, 
      height: scalableHeight,
      flexDirection: 'column',
      justifyContent: 'center'
    };

    // FADE SECTION
    const fullFadeOutSize = Math.max(tegScalableSectionExpandedHeight, tegFadeableSectionHeight);
    
    const fadeableSectionOpacity = this.state.scrollY.interpolate({
      inputRange: [0, headerExpHeight - (fullFadeOutSize * 0.75)],
      outputRange: tegLock === 'COLLAPSED' 
        ? [-.1, 1]
        : [1, -.1],
      extrapolate: 'clamp'
    });

    const fadeableAnimStyle = {
      position: 'absolute',
      left: headerLeftGap,
      right: headerRightGap,
      bottom: headerBottomGap,
      opacity: fadeableSectionOpacity,
      height: tegFadeableSectionHeight
    };

    // CHILDREN MANIPULATION
    React.Children.only(children);

    const childScrollCallback = (children.props || {}).onScroll;
    const childScrollEventThrottle = (children.props || {}).scrollEventThrottle || 16;    

    const lock = tegLock;
    const contentPaddingTop = (!lock && headerExpHeight)  
      || (lock === 'EXPANDED' && headerExpHeight)
      || (lock === 'COLLAPSED' && headerColHeight)
    const contentMinHeight = (!lock && (windowHeight + headerExpHeight - headerColHeight - headerTopGap - headerBottomGap))
      || (lock === 'EXPANDED' && windowHeight)
      || (lock === 'COLLAPSED' && windowHeight)

    const clonedChildren = React.cloneElement(children, {  
      style: {   },
      contentContainerStyle: {
        paddingTop: contentPaddingTop,
        minHeight: contentMinHeight
      },
      onScroll: !!lock ? childScrollCallback : Animated.event(
        [{ nativeEvent: { contentOffset: { y: this.state.scrollY }}}],
        { listener: childScrollCallback }
      ),
      scrollEventThrottle:childScrollEventThrottle,
    });
    
    const leftComponentStyle = {
      position: 'absolute', 
      top: headerTopGap, 
      left: headerLeftGap
    };

    const rightComponentStyle = {
      position: 'absolute',
      top: headerTopGap,
      right: headerRightGap
    };

    const dividerStyle = {
      position: 'absolute', 
      bottom: 0, 
      width: screenWidth, 
      height: 1
    };
    
    return (
      <View style={[style]}>

        {clonedChildren}

        <View style={{
          width: screenWidth,
          position: 'absolute',
          top: 0,
          backgroundColor: tegBackgroundColor
        }}
        pointerEvents='box-none'
        >
          <Animated.View style={headerAnimStyle} pointerEvents='box-none'>
          
            <Animated.View style={[fadeableAnimStyle]} pointerEvents='box-none'>
              {!TegFadeableSection ? null : <TegFadeableSection/>}
            </Animated.View>

            <Animated.View style={scalableContainerAnimStyle} pointerEvents='box-none'>
              <Animated.View style={[scalableAnimStyle]} pointerEvents='box-none'>
                {!TegScalableSection ? null : <TegScalableSection/>}
              </Animated.View>
            </Animated.View>
            
            <View style={leftComponentStyle} pointerEvents='box-none'>
              {!TegTopLeftSection ? null : <TegTopLeftSection/>}
            </View>

            <View style={rightComponentStyle} pointerEvents='box-none'>
              {!TegTopRightSection ? null : <TegTopRightSection/>}
            </View>

          </Animated.View>

          <View pointerEvents='box-none' style={[dividerStyle, tegDividerStyle]}/>
        </View>

      </View>
    );
  }
}


TegCollapsableScrollHeader.propTypes = {
  tegTopLeftSection:                  PropTypes.func,
  tegTopRightSection:                 PropTypes.func,

  tegScalableSection:                 PropTypes.func,
  tegScalableSectionExpandedHeight:   PropTypes.number.isRequired,
  tegScalableSectionCollapsedHeight:  PropTypes.number.isRequired,

  tegFadeableSection:                 PropTypes.func,
  tegFadeableSectionHeight:           PropTypes.number.isRequired,

  tegDividerStyle:                    PropTypes.any,

  tegLock:                            PropTypes.oneOf(['EXPANDED', 'COLLAPSED', false]).isRequired,

  tegStyle:                           PropTypes.shape({
    tegPaddingLeft:                     PropTypes.number,
    tegPaddingRight:                    PropTypes.number,
    tegPaddingTop:                      PropTypes.number,
    tegPaddingBottom:                   PropTypes.number,
    tegPadding:                         PropTypes.number,
    tegBackgroundColor:                 PropTypes.string
  }),
};

TegCollapsableScrollHeader.defaultProps = {
  tegLock: false
}

export default TegCollapsableScrollHeader;
