
import React from 'react';

export interface TegCollapsableScrollHeaderStyle {
  tegPaddingLeft: number;
  tegPaddingRight:  number;
  tegPaddingTop:  number;
  tegPaddingBottom: number;
  tegPadding: number;
  tegBackgroundColor: string;
}

export interface TegCollapsableScrollHeaderProps {
  tegTheme?:                          string;

  tegStyle:                           TegCollapsableScrollHeaderStyle;
  tegDividerStyle:                    any;

  tegTopLeftSection?:                 React.Component;
  tegTopRightSection?:                React.Component;

  tegScalableSection:                 React.Component;
  tegScalableSectionExpandedHeight:   number;
  tegScalableSectionCollapsedHeight:  number;  

  tegFadeableSection:                 React.Component;
  tegFadeableSectionHeight:           number;  

  tegLock:                            'EXPANDED'|'COLLAPSED'|boolean;
}

export interface TegCollapsableScrollHeaderExtra {}

export default class TegCollapsableScrollHeader extends React.Component<TegCollapsableScrollHeaderProps> {}
