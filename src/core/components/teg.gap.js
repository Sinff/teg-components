import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

class TegGap extends React.Component {

  render() {    
    const dividerStyle = {
      height: this.props.tegGap,
      width: this.props.tegGap
    };

    return <View style={dividerStyle}></View>
  }
}

TegGap.propTypes = {
  tegGap: PropTypes.number.isRequired
}


export default TegGap;
