import React from 'react';

export interface TegLazyContentStyle {
}

export interface TegLazyContentProps {
  tegCurrentView: 'LOADING' | 'SUCCESS' | 'FAILURE';
  tegFailureSection: React.Component|(()=>React.Component);
  tegLoadingSection: React.Component|(()=>React.Component);
}

export default class TegLazyContent extends React.Component<TegLazyContentProps> {}
