import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import AniFadeView from '../animations/ani.fade';

/**
 * Component that switches between three different components:
 *  tegCurrentView == 'FAILURE': Renders the tegFailureSection
 *  tegCurrentView == 'LOADING': Renders the tegLoadingSection
 *  tegCurrentView == 'SUCCESS': Renders the children component  
 */
class TegLazyContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      previousView: 'LOADING'
    };

    this.didSwitchToSuccess = this.didSwitchToSuccess.bind(this);
    this.didSwitchToFailure = this.didSwitchToFailure.bind(this);
    this.didSwitchToLoading = this.didSwitchToLoading.bind(this);
    this.didSwitchContent = this.didSwitchContent.bind(this);
  }

  didSwitchContent() {
    const isShowingContent = this.props.tegCurrentView === 'SUCCESS';
    const isShowingLoading = this.props.tegCurrentView === 'LOADING';
    const isShowingFailure = this.props.tegCurrentView === 'FAILURE';

    if(isShowingContent)
      this.didSwitchToSuccess();
    if(isShowingLoading)
      this.didSwitchToLoading();
    if(isShowingFailure)
      this.didSwitchToFailure();
  }

  didSwitchToSuccess() {
    if(this.state.previousView !== 'SUCCESS')
      this.setState({ previousView: 'SUCCESS' });
  }

  didSwitchToLoading() {
    if(this.state.previousView !== 'LOADING')
      this.setState({ previousView: 'LOADING' });
  }

  didSwitchToFailure() {
    if(this.state.previousView !== 'FAILURE')
      this.setState({ previousView: 'FAILURE' });
  }

  render() {
    const { 
      tegCurrentView, 
      tegLoadingSection: TegLoadingSection, 
      tegFailureSection: TegFailureSection, 
      ...rest 
    } = this.props;
    
    const containerStyle = {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    };

    const isShowingContent = this.props.tegCurrentView === 'SUCCESS';
    const shouldRemoveSuccess = this.props.tegCurrentView !== 'SUCCESS' && this.state.previousView !== 'SUCCESS' ;
    
    const isShowingLoading = this.props.tegCurrentView === 'LOADING';
    const shouldRemoveLoading = this.props.tegCurrentView !== 'LOADING'&& this.state.previousView !== 'LOADING';

    const isShowingFailure = this.props.tegCurrentView === 'FAILURE';
    const shouldRemoveFailure = this.props.tegCurrentView !== 'FAILURE'&& this.state.previousView !== 'FAILURE';
  
    return (
      <View style={{flex: 1}}>
        {
          shouldRemoveSuccess
          ? null
          : (
            <AniFadeView 
              duration={250}
              isShowing={isShowingContent}
              didFinishAnimation={this.didSwitchContent}
              style={{flex: 1}}
            >{this.props.children}</AniFadeView>
          )
        }
        {
          shouldRemoveFailure
          ? null
          : (
            <AniFadeView 
              duration={250}
              isShowing={isShowingFailure} 
              didFinishAnimation={this.didSwitchContent}
              style={containerStyle}
            >
              <TegFailureSection {...rest}/>
            </AniFadeView>
          )
        }
        
        {
          shouldRemoveLoading
          ? null
          : (
            <AniFadeView 
              duration={250}
              isShowing={isShowingLoading} 
              didFinishAnimation={this.didSwitchContent}
              style={containerStyle}
            >
              <TegLoadingSection {...rest}/>
            </AniFadeView>
          )
        }

        { ( /** 
            * If there are multiple views being rendered, we cover everything with an invisible 
            * overlay to prevent interaction during the transition
            */ 
            (shouldRemoveSuccess ? 0 : 1) + 
            (shouldRemoveFailure ? 0 : 1) + 
            (shouldRemoveLoading ? 0 : 1)
          ) < 2 
          ? null
          : <View style={[containerStyle, {backgroundColor: '#0000'}]} />
        }

      </View>
    )
  }
}

TegLazyContent.propTypes = {
  tegCurrentView: PropTypes.string,
  tegFailureSection: PropTypes.func.isRequired,
  tegLoadingSection: PropTypes.func.isRequired,
};

TegLazyContent.defaultProps = { 
  tegCurrentView: 'LOADING'
};

export default TegLazyContent;
