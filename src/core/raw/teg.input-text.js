import React from 'react';
import PropTypes from 'prop-types';
import { TextInput } from 'react-native';
import { addFocusable, delFocusable, doBlur, doFocus, doFocusNext, hasNext } from '../services/srv.focus';
import { MaskService } from '../services/srv.mask';

class TegInputText extends React.PureComponent {
  constructor(props) {
    super(props);
    
    this.didChangeText = this.didChangeText.bind(this);
    this.didFocus = this.didFocus.bind(this);
    this.didBlur = this.didBlur.bind(this);
    this.didInputMount = this.didInputMount.bind(this);
    this.didSubmitEditing = this.didSubmitEditing.bind(this);
    this.didPressKey = this.didPressKey.bind(this);
  }

  didInputMount(input) {
    this.textInput = input;
    
    if(!input)
      return;

    addFocusable(
      this.props.tegCategory,
      {
        id: this.props.tegName,
        focused: this.props.tegAutoFocus,
        didFocus: this.didFocus,
        didBlur: this.didBlur,
        next: this.props.tegNextFocus
      }
    );
  }

  componentDidMount() {
  }

  componentWillUnmount() {
    delFocusable(this.props.tegCategory, this.props.tegName);
    this.textInput = null;
  }

  didFocus() {
    if(!this.props.tegEnabled)
      return doFocusNext(this.props.tegCategory, this.props.tegName);
    
    if(this.focused)
      return;

    this.focused = true;
    setTimeout(() => this.textInput && this.textInput.focus(), 100);
    this.props.tegDidFocus && this.props.tegDidFocus();
  }

  didBlur() {
    const { tegName, tegCategory, tegDidBlur } = this.props;
    
    if(!this.focused)
      return;

    this.focused = false;    
    
    if(!hasNext(tegCategory, tegName)) 
      this.textInput && this.textInput.blur();

    tegDidBlur && tegDidBlur();
  }

  didChangeText(text) {
    const { tegDidChangeValue, tegMask } = this.props;
    const rawValue = tegMask ? MaskService.getRawValue(text, tegMask) : text;
    tegDidChangeValue(rawValue);
  }

  didSubmitEditing(event) {
    const { tegCategory, tegName, tegSubmitEditing } = this.props;
    doFocusNext(tegCategory, tegName);
    tegSubmitEditing && tegSubmitEditing(event);
  }

  didPressKey(event) {
    this.props.tegDidPressKey && this.props.tegDidPressKey(nativeEvent);
  }

  render() {
    const { 
      tegPlaceholder,
      tegPlaceholderColor, 
      tegSelectionColor,
      tegEnabled,
      style,
      tegSecureTextEntry,
      tegInputContentType,
      tegKeyboardType,
      tegMaxLength,
      tegReturnKeyType,
      tegValue,
      tegMask
    } = this.props;

    const _result = !!tegMask 
      ? MaskService.applyMask(tegValue, tegMask) 
      : { rawValue: tegValue, maskedValue: tegValue };

    const tegMaskedValue = _result.maskedValue;
 
    return ( 
      <TextInput
        ref={this.didInputMount}
        onFocus={() => doFocus(this.props.tegCategory, this.props.tegName)}
        onBlur={() =>  doBlur(this.props.tegCategory, this.props.tegName)}
        placeholder={tegPlaceholder}
        placeholderTextColor={tegPlaceholderColor}
        underlineColorAndroid="transparent"
        selectionColor={tegSelectionColor}
        style={style}
        editable={tegEnabled}
        onKeyPress={this.didPressKey}
        onChangeText={this.didChangeText}
        value={tegMaskedValue}
        maxLength={tegMaxLength}
        onSubmitEditing={this.didSubmitEditing}
        blurOnSubmit={false}
        textContentType={tegInputContentType}
        keyboardType={tegKeyboardType}
        returnKeyType={tegReturnKeyType}
        secureTextEntry={tegSecureTextEntry}
      ></TextInput>
    )
  }
}

TegInputText.propTypes = { 
  tegValue:                       PropTypes.string.isRequired,
  tegPlaceholder:                 PropTypes.string.isRequired,
  tegPlaceholderColor:            PropTypes.string,
  tegSelectionColor:              PropTypes.string,

  tegMaxLength:                   PropTypes.number,
  tegEnabled:                     PropTypes.bool,

  tegDidChangeValue:              PropTypes.func.isRequired,
  tegDidFocus:                    PropTypes.func,
  tegDidBlur:                     PropTypes.func,

  tegSecureTextEntry:             PropTypes.bool,
  tegInputContentType:            PropTypes.string,
  tegKeyboardType:                PropTypes.string,
  tegReturnKeyType:               PropTypes.string,

  tegName:                        PropTypes.string.isRequired,
  tegCategory:                    PropTypes.string.isRequired,
  tegAutoFocus:                   PropTypes.bool,
  tegNextFocus:                   PropTypes.string,
};

TegInputText.defaultProps = { 
  tegEnabled: true,
  tegReturnKeyType: 'done'
}

export default TegInputText;
