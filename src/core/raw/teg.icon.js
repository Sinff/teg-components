import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { ConfigurationService } from '../services/srv.configuration';


class TegIcon extends React.Component {

  render() {
    const IconRenderer = ConfigurationService.getIconRenderer();
  
    return (
      <View style={this.props.style}>
        <IconRenderer {...this.props} />
      </View>
    )
  }
}

TegIcon.propTypes = { 
  tegIcon: PropTypes.string.isRequired,
  tegSource: PropTypes.string.isRequired,
  tegSize: PropTypes.number.isRequired,
  tegColor: PropTypes.string.isRequired,
};

export default TegIcon;
