import React from 'react';
import { View, BackHandler } from 'react-native';
import PropTypes from 'prop-types';

const noStackRegisteredError = `
The overlay stack functions are't set. It means that no TegOverlayStack component was found in your component tree. 
To solve this issue you must add a single <TegOverlayStack> component in your component tree. Since it will be used as the hook to draw the dialog overlays, it should be close to the topmost component of yours.
E.g.:
  <View>                <-- My Topmost View
    <Navigator/>        <-- Any kind of navigator or container component of yours
    <TegOverlayStack/>  <-- (HERE) The stack hook to draw overlays
  </View>
`;

let overlayId = 0;

let _addToStack;
let _popFromStack;

function addRendererToStack(name, renderer, didTouchBackButton) {
  if(!_addToStack || !_popFromStack)
    throw Error(noStackRegisteredError);

  _addToStack({name, render: renderer, didTouchBackButton})
}

function popRendererFromStack(name) {
  if(!_addToStack || !_popFromStack)
    throw Error(noStackRegisteredError);

  _popFromStack({name});
}


export class TegOverlay extends React.Component {
  
  constructor(props) {
    super(props);
    this.contentRendererDelegate = this.contentRendererDelegate.bind(this);
    this.overlayId = (++overlayId) + '-Overlay';
  }

  contentRendererDelegate () {
    return !this.childrenReference
      ? null
      : <View style={{flex: 1, backgroundColor: this.props.backgroundColor, flexDirection: 'column'}}>
          {this.childrenReference()}
        </View>
  }

  componentWillUnmount() {
    _popFromStack(this.overlayId);
  }

  componentDidUpdate() {
    this.doAfterUpdate(); 
  }

  render() {    
    const { isShowing, children, didTouchBackButton } = this.props;
    
    if (isShowing) {
      this.childrenReference = () => children;
      this.doAfterUpdate = () => addRendererToStack(this.overlayId, this.contentRendererDelegate, didTouchBackButton);

    } else {
      this.childrenReference = null;
      this.doAfterUpdate = () => popRendererFromStack(this.overlayId);
    }

    return null;
  }
}


let _handleBackButton;
function configureHookCallbacks(addToStack, popFromStack, handleBackButton) {
  if(_addToStack !== undefined && addToStack !== undefined )
    throw new Error('Cannot add multiple overlay callbacks at the same time. Are you trying to use more than one TegOverlayStack?');

  if(_popFromStack !== undefined && popFromStack !== undefined )
    throw new Error('Cannot add multiple overlay callbacks at the same time. Are you trying to use more than one TegOverlayStack?');

  if(handleBackButton)
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
  else if(_handleBackButton)
    BackHandler.removeEventListener('hardwareBackPress', _handleBackButton);

  _handleBackButton = handleBackButton;
  _addToStack = addToStack;
  _popFromStack = popFromStack;
}

TegOverlay.propTypes = {
  isShowing: PropTypes.bool.isRequired,
  didTouchBackButton: PropTypes.func
}

TegOverlay.defaultProps = {
  isShowing: false,
}


/****** STACK *******/

export class TegOverlayStack extends React.Component {
  
  constructor (props) {
    super(props);

    this.state = { stack: [] };

    this.addToStack = this.addToStack.bind(this);
    this.popFromStack = this.popFromStack.bind(this);
    this.handleBackButton = this.handleBackButton.bind(this);
  }

  handleBackButton() {
    const { stack } = this.state;
    
    for(let i = stack.length - 1; i >= 0; i--)
      if(stack[i].didTouchBackButton) 
        return (stack[i].didTouchBackButton(), true);

    // We return true when there is any overlay in the stack so we can prevent the 
    // android's back button press event from bubbling 
    return stack.length > 0;
  }

  addToStack(cmp) {
    let newStack = [].concat(this.state.stack);

    // Add the component to the stack only if it isn't there yet
    for(let i = 0; i < newStack.length; i++)
      if(newStack[i].name === cmp.name) 
        return this.setState({stack: newStack});

    this.setState({stack: newStack.concat([cmp])});
  } 

  popFromStack(cmp) {
    let newStack = [];

    // Breaks the stack removing the given component and all itens above.
    for(let i = 0; i < this.state.stack.length; i++)
      if(this.state.stack[i].name == cmp.name) { 
        newStack = this.state.stack.slice(0, i);      
        break;
      }
    
    this.setState({stack: newStack});
  }

  componentWillMount() {
    configureHookCallbacks(this.addToStack, this.popFromStack, this.handleBackButton);
  }

  componentWillUnmount() {
    configureHookCallbacks(undefined, undefined, undefined);
  }  

  render() {
    const { stack } = this.state;    
    const { style } = this.props;

    if (stack.length == 0)
      return null;
      
    const overlayStyle = {
      position: 'absolute',
      left: 0, top: 0, right: 0, bottom: 0,
      backgroundColor: 'transparent',
      zIndex: 2,
      flex: 1
    };

    return (
      <View style={[overlayStyle, style]}>
        {stack.map(cmp => <View key={cmp.name} style={overlayStyle}>{cmp.render()}</View>)}
      </View> 
    );
  }
}

TegOverlayStack.propTypes = {
  style: PropTypes.any
}

