import React from 'react';
import { TextInput } from 'react-native';


export interface TegInputTextProps {

  /** Input placeholder */
  tegPlaceholder: string;
  tegPlaceholderColor: string;
  tegSelectionColor: string;

  
  tegValue: string;
  /**
   * Passe to the InputText's returnKeyType property
   */
  tegReturnKeyType?: string;
  
  /** Hide text for security purpose */
  tegSecureTextEntry?: boolean;
  /** Passed to InputText.textContentType */
  tegInputContentType?: string;
  /** Passed to InputText.keyboardType */
  tegKeyboardType?: string;
  /** Maximum number of characters allowed */
  tegMaxLength?: number;
  
  
  tegDidChangeValue: (value: string) => void;
  
  /** Function called on focus */
  tegDidFocus?: () => void;
  /** Function called on blur */
  tegDidBlur?: () => void;

  tegEnabled?: boolean;

  /** The component unique name (Required) */
  tegName: string;
  /** The component focus category. It is used to group inputs in a form (Required) */
  tegCategory: string;
  /** Whether or not the component should request focus on mount */
  tegAutoFocus?: boolean;
  /** The next component to receive focus on blur */
  tegNextFocus?: boolean;
  tegSubmitEditing?: (evt: any) => void;

}


export default class TegInputText extends React.Component<TegInputTextProps> {}


