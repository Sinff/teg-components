import React from 'react';

export interface TegIconProps {
  tegIcon: string;
  tegSource: string;
  tegSize: number;
  tegColor: string;
}

export default class TegIcon extends React.Component<TegIconProps> {}
