import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { TegOverlay } from '../raw/teg.overlay';
import AniBottomView from '../animations/ani.bottom';
import AniFadeView from '../animations/ani.fade';


class TegFrameOverlay extends React.Component {
  constructor(props) {
    super(props);
    this.state = { tegIsShowing: false }
    this.didFinishDialogAnimation = this.didFinishDialogAnimation.bind(this);
  }

  didFinishDialogAnimation(tegIsShowing) {
    if(!tegIsShowing)
      this.setState({ tegIsShowing: false });
  }

  componentDidUpdate(prevProps) {
    if(prevProps.tegIsShowing !== this.props.tegIsShowing && this.props.tegIsShowing)
      this.setState({ tegIsShowing: true });
  }

  render() {    
    const { tegIsShowing, tegAnimationDuration, tegDidTouchBackButton, tegOverlayColor, tegContentAlignment } = this.props;

    const contentStyle = {
      flex: 1,
      backgroundColor: 'transparent',
      flexDirection: 'row',
      justifyContent: tegContentAlignment,
      alignItems: tegContentAlignment,
    }

    const overlayStyle = {
      flex: 1,
      backgroundColor: tegOverlayColor
    };

    return (
      <TegOverlay 
        isShowing={this.state.tegIsShowing} 
        didTouchBackButton={tegDidTouchBackButton}
      >
        <AniFadeView duration={tegAnimationDuration} isShowing={tegIsShowing} style={overlayStyle}>
          <AniBottomView 
            style={contentStyle}
            duration={tegAnimationDuration}
            didFinishAnimation={() => this.didFinishDialogAnimation(tegIsShowing)} 
            isShowing={tegIsShowing}
          >
            <View style={{ flex: 1 }}>
              {this.props.children}
            </View>
            
          </AniBottomView>
        </AniFadeView>
      </TegOverlay>
    );
  }
}

TegFrameOverlay.propTypes = {
  tegIsShowing: PropTypes.bool.isRequired,
  tegDidTouchBackButton: PropTypes.func,
  tegAnimationDuration: PropTypes.number.isRequired,
  tegOverlayColor: PropTypes.string.isRequired,
  tegContentAlignment: PropTypes.oneOf(['flex-end', 'center']).isRequired
}

TegFrameOverlay.defaultProps = {
  tegIsShowing: false,
  tegAnimationDuration: 500,
  tegOverlayColor: '#0003',
  tegContentAlignment: 'center'
}

export default TegFrameOverlay;
