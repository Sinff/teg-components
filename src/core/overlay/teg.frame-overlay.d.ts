import React from 'react';

export interface TegFrameOverlayStyle {
}

export interface TegFrameOverlayProps {
  tegIsShowing:           boolean;
  tegDidTouchBackButton:  () => void;
  tegAnimationDuration:   number;
  tegOverlayColor:        string;
  tegContentAlignment:    'flex-end' | 'center';
}

export interface TegFrameOverlayExtra {
}

export default class TegFrameOverlay extends React.Component<TegFrameOverlayProps> {}
