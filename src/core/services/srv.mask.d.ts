/**
 * The TegMask is composed by an array that applies the values in each position to
 * the respectivie character in the same position. The values can be an RegExp that
 * validates the character or a string that will be used as a constant.
 * 
 * E.g.:
 * const mask = ['(', /[0-9]/, /[0-9]/, /[0-9]/, ')']
 * MaskService.applyMask('1', mask);    // yields to '(1'
 * MaskService.applyMask('12', mask);   // yields to '(12'
 * MaskService.applyMask('123', mask);  // yields to '(123)'
 */
export type TegMask = (RegExp|string)[];

declare class MaskServiceCls {
  getRawValue(value: string, mask: TegMask): string;
  applyMask(value: string, mask: TegMask|((value: string)=>TegMask)): { 
    maskedValue: string,
    rawValue: string
  };
  registerMask(name: string, mask: TegMask): void;
  getMask(name: string): TegMask;
}

export const MaskService = new MaskServiceCls();
