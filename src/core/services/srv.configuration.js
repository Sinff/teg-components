const _configuration = {
  IconRenderer: undefined
};

const configureIconRenderer = (renderer) => {
  _configuration.IconRenderer = renderer;
}

const getIconRenderer = () => _configuration.IconRenderer

export const ConfigurationService = {
  configureIconRenderer: configureIconRenderer,
  getIconRenderer: getIconRenderer
}