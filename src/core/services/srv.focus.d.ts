export interface Focusable {
  tag: string; 
  id: string;
  didFocus: Function; 
  didBlur: Function;
  next: string;
  focused: boolean;  
}

export function addFocusable(tag: string, focusable: Focusable): void;
export function delFocusable(tag: string, id: string): void;
export function doFocus(tag: string, id: string): void;
export function doBlur(tag: string, id: string): void;
export function doFocusNext(tag: string, currFocusId: string): void;
export function isFocused(tag: string, id: string): boolean;
export function hasNext(tag: string, id: string): boolean;

export class FocusServiceCls {
  addFocusable: (tag: string, focusable: Focusable) => void;
  delFocusable: (tag: string, id: string) => void;
  doFocus: (tag: string, id: string) => void;
  doBlur: (tag: string, id: string) => void;
  doFocusNext: (tag: string, currFocusId: string) => void;
  isFocused: (tag: string, id: string) => boolean;
  hasNext: (tag: string, id: string) => boolean
}

export const FocusService = new FocusServiceCls();
