import React from 'react';

interface IconRendererProps {
  tegIcon: string;
  tegSource: string;
  tegSize: number;
  tegColor: string;
  [key: string]: any;
}

export const ConfigurationService: {
  configureIconRenderer: ( renderer: (props: IconRendererProps) => React.Component ) => void;
  getIconRenderer: () => React.Component
};