
let focusables = {};

const _isFocused = (tag, id) => {
  const focused = _getFocusedForTag(tag);
  return focused && focused.id === id;
}

const _getFocusableForTagAndId = (tag, id) => {
  const cmps = focusables[tag] = (focusables[tag] || []);
  
  for(let i = 0; i < cmps.length; i++)
    if(cmps[i].id === id)
      return cmps[i]; 

}

const _getFocusedForTag = (tag) => {
  const cmps = focusables[tag] = (focusables[tag] || []);

  for(let i = 0; i < cmps.length; i++)
    if(cmps[i].focused)
      return cmps[i]; 

}

const _addFocusable = (cmp) => {
  const { tag, id } = cmp;
  const cmps = focusables[tag] = (focusables[tag] || []);

  for(let i = 0; i < cmps.length; i++)
    if(cmps[i].id === id)
      return; 

  cmps.push(cmp);
}

const _delFocusable = (tag, id) => {
  const cmps = focusables[tag] = (focusables[tag] || []);
  let index = -1;

  for(let i = 0; i < cmps.length; i++)
    if(cmps[i].id === id) {
      index = i;
      break
    }

  if(index < 0)
    return;

  cmps.splice(index, 1);
}

const addFocusable = (tag, focusable) => {
  _addFocusable({
    tag, 
    id: focusable.id, 
    didFocus: focusable.didFocus, 
    didBlur: focusable.didBlur,
    next: focusable.next,
    focused: false,
  });

  if(focusable.focused)
    doFocus(tag, focusable.id);
}

const delFocusable = (tag, id) => {
  _delFocusable(tag, id);
}

const doFocus = (tag, id) => {
  const focused = _getFocusedForTag(tag);

  if(focused) {    
    if(focused.id === id) 
      return;
    
    focused.focused = false;
    focused.didBlur();
  }

  const toFocus = _getFocusableForTagAndId(tag, id);

  if(!toFocus)
    throw new Error(`No focusable component found for tag "${tag}" and id "${id}"`);

  toFocus.focused = true;
  toFocus.didFocus();
}

const doBlur = (tag, id) => {
  const toBlur = _getFocusableForTagAndId(tag, id);

  if(!toBlur.focused)
    return;

  toBlur.focused = false;
  toBlur.didBlur();
}

const doFocusNext = (tag, currFocusId) => {
  const current = _getFocusableForTagAndId(tag, currFocusId);
  current && doBlur(tag, currFocusId);
  current && current.next && doFocus(tag, current.next);
}

const hasNext = (tag, id) => {
  const current = _getFocusableForTagAndId(tag, id);
  return current && current.next;
}

const isFocused = _isFocused;

const FocusService = {
  addFocusable,
  delFocusable,
  doFocus,
  doBlur,
  doFocusNext,
  isFocused,
  hasNext
};

export {
  addFocusable,
  delFocusable,
  doFocus,
  doBlur,
  doFocusNext,
  isFocused,
  hasNext,
  FocusService
};