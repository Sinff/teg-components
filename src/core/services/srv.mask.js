const _masks = {};


function getRawValue(value, mask) {
  mask = typeof mask == 'function' ? mask(value) : mask;
  mask = typeof mask == 'string' ? getMask(mask) : mask;

  let rawValue = '';

  for(let i = 0; i < mask.length; i++) {
    const char = value.charAt(i) || '';
    const charMask = mask[i];
  
    if(typeof charMask == 'string' && charMask === char)  
      continue;

    if(typeof charMask == 'string' && charMask !== char)
      rawValue += char;
    
    if(charMask instanceof RegExp && charMask.test(char))
      rawValue += char;
  }

  return rawValue;
}

function applyMask(value, mask) {
  mask = typeof mask == 'function' ? mask(value) : mask;
  mask = typeof mask == 'string' ? getMask(mask) : mask;
  rawValue = getRawValue((value || '') + '', mask);

  let maskedValue = '';
  let charIdx = 0

  if(rawValue.length == 0)
    return { maskedValue: '', rawValue: '' };

  for(let i = 0; i < mask.length; i++) {
    const char = rawValue.charAt(charIdx) || '';
    const charMask = mask[i];

    if(rawValue.length <= charIdx)
      break;
  
    if(typeof charMask == 'string') {
      maskedValue += charMask;

      if(charMask == char) 
        ++charIdx;

      
    } else if(charMask instanceof RegExp) {

      if(char !== '' && charMask.test(char)) 
        (maskedValue += char, ++charIdx);
    }
  }

  return { 
    maskedValue,
    rawValue
  };

}

function getMask(name) {
  return _masks[name];
}

function registerMask(name, mask) {
  _masks[name] = mask;
}

export const MaskService = {
  getRawValue: getRawValue,
  applyMask: applyMask,
  registerMask: registerMask,
  getMask: getMask
};

